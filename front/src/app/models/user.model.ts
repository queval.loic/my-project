export interface UserModel {
  name?: string;
  director?: number;
  pool?: boolean;
  roomNumber?: number;
  stars?: number;
}
