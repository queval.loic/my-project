import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersDetailComponent } from './users-detail.component';



@NgModule({
  declarations: [UsersDetailComponent],
  imports: [
    CommonModule
  ]
})
export class UsersDetailModule { }
