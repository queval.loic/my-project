import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UsersComponent} from './users.component';


const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children: [
      {
        path: 'list',
        loadChildren: () => import('./users-list/users-list.module').then(m => m.UsersListModule),
      },
      {
        path: ':id',
        loadChildren:() => import('./users-detail/users-detail.module').then(m => m.UsersDetailModule),
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}
