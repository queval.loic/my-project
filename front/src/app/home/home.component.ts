import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {takeUntil, tap} from 'rxjs/operators';
import {UserModel} from '../models/user.model';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  user: UserModel;
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private userService: UserService,
  ) {
  }

  ngOnInit() {
    this.getHostels();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getHostels() {
    this.userService.getHostels$()
      .pipe(
        takeUntil(this.destroy$),
        tap(() => console.log('toto')),
        tap((user: UserModel) => this.user = user),
      )
      .subscribe();
  }

}
