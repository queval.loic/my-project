import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserModel} from '../models/user.model';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private afs: AngularFirestore,
  ) {
  }

  getHostels$(): Observable<UserModel> {
  return this.afs.collection('hostels').doc('EHnIAwqxcA5D9NxEOG75').valueChanges();
  }

}
